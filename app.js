const express = require('express');
const mongoose = require('mongoose');
const bodyPaser = require('body-parser');

const app = express();
const port = 3000;

// middleware
app.use(bodyPaser.json());

const postsRoutes = require('./routes/posts')
const studentsRoutes = require('./routes/students')

app.use('/posts', postsRoutes);
app.use('/students', studentsRoutes);

app.get('/', (req, res) => {
    res.send('home');
});

mongoose.connect("mongodb://localhost:27017/backendapi", { useNewUrlParser: true }, () => {
    console.log('Connected to db!')
});

app.listen(port, () => {
    console.log(`Server running at port: ${port}`)
});
