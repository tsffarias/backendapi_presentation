const express = require('express');
const router = express.Router();

const Students = require('../models/Student')

router.get('/', async (req, res) => {
    try {
        const student = await Students.find();
        res.json(student);
    } catch (err) {
        res.json({message: err});
    }
});

router.post('/', (req, res) => {
    // console.log(req.body);

    const student = new Students({
        name: req.body.name,
        age: req.body.age
    });

    student.save().then(data => {
        res.json(data);
    }).catch(err => {
        res.json({message: err})
    });
});

module.exports = router;